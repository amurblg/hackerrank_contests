/*
* https://www.hackerrank.com/challenges/sherlock-and-anagrams/problem
* */

package Algorithms;

import java.util.Arrays;
import java.util.Scanner;

public class SherlockAndAnagrams {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
//        Scanner scanner = new Scanner("5\n" +
//                "ifailuhkqq\n" +
//                "hucpoltgty\n" +
//                "ovarjsnrbf\n" +
//                "pvmupwjjjf\n" +
//                "iwwhrlkpek");

//        Scanner scanner = new Scanner("2\n" +
//                "abba\n" +
//                "abcd");
        int t = scanner.nextInt();

        for (int i = 0; i < t; i++) {
            String originalLine = scanner.next();

            int counter = 0;
            for (int j = 0; j < originalLine.length() - 1; j++) {
                for (int k = j + 1; k < originalLine.length(); k++) {
                    String leftPart = originalLine.substring(j, k);
                    for (int l = j + 1; l < originalLine.length(); l++) {
                        for (int m = l + 1; m <= originalLine.length(); m++) {
                            String rightPart = originalLine.substring(l, m);
                            if (leftPart.length() == rightPart.length()) {
                                byte[] left = leftPart.getBytes();
                                byte[] right = rightPart.getBytes();
                                Arrays.sort(left);
                                Arrays.sort(right);
                                if (Arrays.equals(left, right)) {
                                    counter++;
                                    //System.out.println(leftPart + " === " + rightPart);
                                }
                            }
                        }
                    }
                }
            }
            System.out.println(counter);
        }
    }
}
