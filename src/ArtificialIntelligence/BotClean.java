/*
* https://www.hackerrank.com/challenges/botclean?hr_b=1
* */

package ArtificialIntelligence;

import java.util.Scanner;

public class BotClean {
    public static void main(String[] args) {
        //Scanner s = new Scanner(System.in);
        Scanner s = new Scanner("0 1\n" +
                "-b--d\n" +
                "-d--d\n" +
                "--dd-\n" +
                "--d--\n" +
                "----d");
        int currRow = s.nextInt();
        int currCol = s.nextInt();
        s.nextLine();

        char[][] matrix = new char[5][5];
        for (int i = 0; i < 5; i++) {
            matrix[i] = s.nextLine().toCharArray();
        }

        next_move(currRow, currCol, matrix);
    }

    public static void next_move(int posr, int posc, char[][] matrix) {
        if (matrix[posr][posc] == 'd') {
            System.out.println("CLEAN");
        } else {
            CellCoords nextCell = nearestDirtyCellCoords(posr, posc, matrix);
            if (nextCell.getCol() > posc) {
                System.out.println("RIGHT");
            } else if (nextCell.getCol() < posc) {
                System.out.println("LEFT");
            } else {
                if (nextCell.getRow() > posr) {
                    System.out.println("DOWN");
                } else if (nextCell.getRow() < posr) {
                    System.out.println("UP");
                }
            }
        }
    }

    private static CellCoords nearestDirtyCellCoords(int posr, int posc, char[][] matrix) {
        int nearestRow = -1;
        int nearestCol = -1;
        int minDistance = Integer.MAX_VALUE;
        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[0].length; col++) {
                if (posc == col && posr == row) continue;
                if (matrix[row][col] == 'd') {
                    if ((Math.abs(posc - col) + Math.abs(posr - row)) < minDistance) {
                        nearestCol = col;
                        nearestRow = row;
                        minDistance = Math.abs(posc - col) + Math.abs(posr - row);
                    }
                }
            }
        }
        if (nearestCol != -1 && nearestRow != -1) return new CellCoords(nearestRow, nearestCol);
        return null;
    }

    static class CellCoords {
        private int row;
        private int col;

        public int getRow() {
            return row;
        }

        public int getCol() {
            return col;
        }

        public CellCoords(int row, int col) {
            this.row = row;
            this.col = col;
        }
    }
}
