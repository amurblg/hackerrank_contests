/*
* https://www.hackerrank.com/challenges/botcleanv2/problem
* */

package ArtificialIntelligence;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class BotCleanPartiallyObservable {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int posr = s.nextInt();
        int posc = s.nextInt();
        s.nextLine();
        char[][] matrix = new char[5][5];
        for (int i = 0; i < 5; i++) {
            matrix[i] = s.nextLine().toCharArray();
        }
        next_move(posr, posc, matrix);
    }

    public static void next_move(int posr, int posc, char[][] matrix) {
        List<CellCoord> necessaryCells = new ArrayList<>(Arrays.asList(
                new CellCoord(1, 1),
                new CellCoord(2, 1),
                new CellCoord(3, 1),
                new CellCoord(3, 2),
                new CellCoord(3, 3),
                new CellCoord(2, 3),
                new CellCoord(1, 3),
                new CellCoord(1, 2)
        ));

        if (matrix[posr][posc] == 'd') System.out.println("CLEAN");
        else {
            CellCoord nextCell = getNextCellToMove(matrix, necessaryCells, posr, posc);
            if (nextCell.getPosc() > posc) {
                System.out.println("RIGHT");
            } else if (nextCell.getPosc() < posc) {
                System.out.println("LEFT");
            } else {
                if (nextCell.getPosr() > posr) {
                    System.out.println("DOWN");
                } else {
                    System.out.println("UP");
                }
            }
        }
    }

    /**
     * Determines next cell to move with given matrix, current cell coords, and necessary cells list. Priorities are:
     * - if dirty cells are in the scope - move to the nearest.
     * - if no dirty cells visible, and we are not on the "necessary path" - move to the nearest cell of this path.
     * - if no dirty cells visible, and we are on the "necessary path" - move to the next cell of this path.
     *
     * @param matrix         - current field 5x5 (maximum 8 adjacent cells visible)
     * @param necessaryCells - hardcoded ArrayList with chain of cells coords, having walking which we'll observe all cells of this matrix.
     * @param posr           - current row.
     * @param posc           - current column.
     * @return instance of cell to move to.
     */
    private static CellCoord getNextCellToMove(char[][] matrix, List<CellCoord> necessaryCells, int posr, int posc) {
        List<CellCoord> dirtyCells = new ArrayList<>();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                if (matrix[i][j] == 'd') dirtyCells.add(new CellCoord(i, j));
            }
        }
        if (dirtyCells.size() > 1) {//In this block we need to check whether list of dirty cells includes  those located
            // right on the necessary path. If we have such cells (and also one or more other dirty  cells, located  out
            // of this path) - remove it from list, because we need to clean out of the path cells first. Dirty cells on
            // the necessary pass are of the less priority.
            // See unit test next_move33d424423() for details.
            for (CellCoord dirtyCell : dirtyCells) {
                if (necessaryCells.contains(dirtyCell)) {
                    dirtyCells.remove(dirtyCell);
                    break;
                }
            }
        }
        if (dirtyCells.size() > 0) {//If dirty cell in visibility - go towards it first thing.
            CellCoord nearest = dirtyCells.get(0);
            int minDelta = Math.abs(nearest.getPosr() - posr) + Math.abs(nearest.getPosc() - posc);

            for (CellCoord dirtyCell : dirtyCells) {
                if (Math.abs(dirtyCell.getPosc() - posc) + Math.abs(dirtyCell.getPosr() - posr) < minDelta) {
                    nearest = dirtyCell;
                    minDelta = Math.abs(nearest.getPosr() - posr) + Math.abs(nearest.getPosc() - posc);
                }
            }
            return nearest;
        } else {//If there's no dirty cells visible - continue walk around necessary cells list
            CellCoord currentCell = new CellCoord(posr, posc);
            if (necessaryCells.contains(currentCell)) {//If we're "on the necessary path" right now - just go to the next cell
                for (int i = 0; i < necessaryCells.size(); i++) {
                    if (necessaryCells.get(i).equals(currentCell))
                        return necessaryCells.get((i + 1) % necessaryCells.size());
                }
                return null;
            } else {//Otherwise we need to return on to the path (to the nearest cell of the path, right?)
                CellCoord nearest = necessaryCells.get(0);
                int minDistance = Math.abs(nearest.getPosc() - posc) + Math.abs(nearest.getPosr() - posr);
                for (int i = 0; i < necessaryCells.size(); i++) {
                    if (Math.abs(necessaryCells.get(i).getPosc() - posc) + Math.abs(necessaryCells.get(i).getPosr() - posr) <= minDistance) {
                        nearest = necessaryCells.get(i);
                        minDistance = Math.abs(necessaryCells.get(i).getPosc() - posc) + Math.abs(necessaryCells.get(i).getPosr() - posr);
                    }
                }
                return nearest;
            }
        }
    }
}

class CellCoord {
    private int posr;
    private int posc;

    public CellCoord(int posr, int posc) {
        this.posr = posr;
        this.posc = posc;
    }

    @Override
    public String toString() {
        return String.format("[%d; %d]", posr, posc);
    }

    public int getPosc() {
        return posc;
    }

    public int getPosr() {

        return posr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CellCoord cellCoord = (CellCoord) o;

        if (posr != cellCoord.posr) return false;
        return posc == cellCoord.posc;
    }

    @Override
    public int hashCode() {
        int result = posr;
        result = 31 * result + posc;
        return result;
    }
}