package ArtificialIntelligence;

import static org.junit.Assert.*;

public class BotCleanPartiallyObservableTest {
    @org.junit.Test
    public void next_move() throws Exception {
        char[][] matrix = new char[][]{
                {'d', '-', 'o', 'o', 'o'},
                {'-', 'b', 'o', 'o', 'o'},
                {'o', 'o', 'o', 'o', 'o'},
                {'o', 'o', 'o', 'o', 'o'},
                {'o', 'o', 'o', 'o', 'o'}
        };

        int posr = 1;
        int posc = 1;
        BotCleanPartiallyObservable.next_move(posr, posc, matrix);
    }

    @org.junit.Test
    public void next_move10() throws Exception {
        char[][] matrix = new char[][]{
                {'-', '-', 'o', 'o', 'o'},
                {'b', '-', 'o', 'o', 'o'},
                {'-', '-', 'o', 'o', 'o'},
                {'o', 'o', 'o', 'o', 'o'},
                {'o', 'o', 'o', 'o', 'o'}
        };

        int posr = 1;
        int posc = 0;
        BotCleanPartiallyObservable.next_move(posr, posc, matrix);
    }

    @org.junit.Test
    public void next_move20() throws Exception {
        char[][] matrix = new char[][]{
                {'-', '-', 'o', 'o', 'o'},
                {'-', '-', 'o', 'o', 'o'},
                {'b', '-', 'o', 'o', 'o'},
                {'o', 'o', 'o', 'o', 'o'},
                {'o', 'o', 'o', 'o', 'o'}
        };

        int posr = 2;
        int posc = 0;
        BotCleanPartiallyObservable.next_move(posr, posc, matrix);
    }

    @org.junit.Test
    public void next_move20d01() throws Exception {
        char[][] matrix = new char[][]{
                {'-', 'd', 'o', 'o', 'o'},
                {'-', '-', 'o', 'o', 'o'},
                {'b', '-', 'o', 'o', 'o'},
                {'o', 'o', 'o', 'o', 'o'},
                {'o', 'o', 'o', 'o', 'o'}
        };

        int posr = 2;
        int posc = 0;
        BotCleanPartiallyObservable.next_move(posr, posc, matrix);
    }

    @org.junit.Test
    public void next_move31() throws Exception {
        char[][] matrix = new char[][]{
                {'o', 'o', 'o', 'o', 'o'},
                {'o', 'o', 'o', 'o', 'o'},
                {'-', '-', '-', 'o', 'o'},
                {'-', 'b', '-', 'o', 'o'},
                {'-', '-', '-', 'o', 'o'}
        };

        int posr = 3;
        int posc = 1;
        BotCleanPartiallyObservable.next_move(posr, posc, matrix);
    }

    @org.junit.Test
    public void next_move31d20() throws Exception {
        char[][] matrix = new char[][]{
                {'o', 'o', 'o', 'o', 'o'},
                {'o', 'o', 'o', 'o', 'o'},
                {'d', '-', '-', 'o', 'o'},
                {'-', 'b', '-', 'o', 'o'},
                {'-', '-', '-', 'o', 'o'}
        };

        int posr = 3;
        int posc = 1;
        BotCleanPartiallyObservable.next_move(posr, posc, matrix);
    }

    @org.junit.Test
    public void next_move32d4243() throws Exception {
        char[][] matrix = new char[][]{
                {'o', 'o', 'o', 'o', 'o'},
                {'o', 'o', 'o', 'o', 'o'},
                {'o', '-', '-', '-', 'o'},
                {'o', '-', 'b', '-', 'o'},
                {'o', '-', 'd', 'd', 'o'}
        };

        int posr = 3;
        int posc = 2;
        BotCleanPartiallyObservable.next_move(posr, posc, matrix);
    }

    @org.junit.Test
    public void next_move32d43() throws Exception {
        char[][] matrix = new char[][]{
                {'o', 'o', 'o', 'o', 'o'},
                {'o', 'o', 'o', 'o', 'o'},
                {'o', '-', '-', '-', 'o'},
                {'o', '-', 'b', '-', 'o'},
                {'o', '-', '-', 'd', 'o'}
        };

        int posr = 3;
        int posc = 2;
        BotCleanPartiallyObservable.next_move(posr, posc, matrix);
    }

    @org.junit.Test
    public void next_move33d4244() throws Exception {
        char[][] matrix = new char[][]{
                {'o', 'o', 'o', 'o', 'o'},
                {'o', 'o', 'o', 'o', 'o'},
                {'o', 'o', '-', '-', '-'},
                {'o', 'o', '-', 'b', '-'},
                {'o', 'o', 'd', '-', 'd'}
        };

        int posr = 3;
        int posc = 3;
        BotCleanPartiallyObservable.next_move(posr, posc, matrix);
    }

    @org.junit.Test
    public void next_move33d424423() throws Exception {
        char[][] matrix = new char[][]{
                {'o', 'o', 'o', 'o', 'o'},
                {'o', 'o', 'o', 'o', 'o'},
                {'o', 'o', '-', 'd', '-'},
                {'o', 'o', '-', 'b', '-'},
                {'o', 'o', 'd', '-', 'd'}
        };
        // Not the best move! bot moves up (since it is nearest dirty cell) - and after that dirty cells in the bottom row
        // ([4;2] and [4;4]) fades out, bot doesn't see them anymore, and it has to take one more lap to see them again!
        int posr = 3;
        int posc = 3;
        BotCleanPartiallyObservable.next_move(posr, posc, matrix);
    }

}