/*
* https://www.hackerrank.com/challenges/saveprincess?hr_b=1
* */

package ArtificialIntelligence;

import java.util.Scanner;

public class BotSavesPrincess {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
//        Scanner s = new Scanner("3\n" +
//                "--p\n" +
//                "-m-\n" +
//                "---");
        int n = Integer.parseInt(s.nextLine());

        char[][] matrix = new char[n][n];
        for (int i = 0; i < n; i++) {
            matrix[i] = s.nextLine().toCharArray();
        }

        displayPathtoPrincess(n, matrix);
    }

    public static void displayPathtoPrincess(int n, char[][] matrix) {
        //System.out.println(Arrays.deepToString(matrix));
        int targetRow = 0;
        int targetCol = 0;
        if (matrix[0][n - 1] == 'p') targetCol = n - 1;
        else if (matrix[n - 1][0] == 'p') targetRow = n - 1;
        else if (matrix[n - 1][n - 1] == 'p') {
            targetRow = n - 1;
            targetCol = n - 1;
        }
        int currRow = n / 2;
        int currCol = n / 2;

        while (true) {
            int deltaRow = targetRow - currRow;
            int deltaCol = targetCol - currCol;
            String nextDirection = "";
            if (deltaRow < 0) {
                nextDirection = "UP";
                currRow--;
            } else if (deltaRow > 0) {
                nextDirection = "DOWN";
                currRow++;
            } else {
                if (deltaCol < 0) {
                    nextDirection = "LEFT";
                    currCol--;
                } else if (deltaCol > 0) {
                    nextDirection = "RIGHT";
                    currCol++;
                } else break;
            }
            System.out.println(nextDirection);
        }
    }
}
