/*
* https://www.hackerrank.com/challenges/saveprincess2?hr_b=1
* */

package ArtificialIntelligence;

import java.util.Scanner;

public class BotSavesPrincess2 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
//        Scanner s = new Scanner("5\n" +
//                "2 3\n" +
//                "-----\n" +
//                "-----\n" +
//                "p--m-\n" +
//                "-----\n" +
//                "-----");
        int n = s.nextInt();
        int r = s.nextInt();
        int c = s.nextInt();
        s.nextLine();

        char[][] matrix = new char[n][n];
        for (int i = 0; i < n; i++) {
            matrix[i] = s.nextLine().toCharArray();
        }
        displayNextMoveToPrincess(n, r, c, matrix);
    }

    public static void displayNextMoveToPrincess(int n, int row, int col, char[][] matrix) {
        int targetRow = 0;
        int targetCol = 0;
        boolean gotcha = false;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] == 'p') {
                    targetRow = i;
                    targetCol = j;
                    gotcha = true;
                    break;
                }
            }
            if (gotcha) break;
        }

        int currRow = row;
        int currCol = col;

        int deltaRow = targetRow - currRow;
        int deltaCol = targetCol - currCol;
        String nextDirection = "";
        if (deltaRow < 0) {
            nextDirection = "UP";
            currRow--;
        } else if (deltaRow > 0) {
            nextDirection = "DOWN";
            currRow++;
        } else {
            if (deltaCol < 0) {
                nextDirection = "LEFT";
                currCol--;
            } else if (deltaCol > 0) {
                nextDirection = "RIGHT";
                currCol++;
            }
        }
        if (!nextDirection.isEmpty()) System.out.println(nextDirection);
    }
}
