/*
* https://www.hackerrank.com/challenges/big-sorting
* */

import java.util.Arrays;
import java.util.Scanner;

public class BigSorting {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = Integer.parseInt(s.nextLine());
        String[] arr = new String[n];
        for (int i = 0; i < n; i++) {
            arr[i] = s.next();
        }
        Arrays.sort(arr, (o1, o2) -> {
            int l1 = o1.length();
            int l2 = o2.length();
            if (l1 == l2) {
                for (int i = 0; i < l1; i++) {
                    if (o1.charAt(i) > o2.charAt(i)) return 1;
                    if (o1.charAt(i) < o2.charAt(i)) return -1;
                }
            } else return l1 - l2;
            return 0;
        });
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < n; i++) {
            buffer.append(arr[i]).append("\n");
        }
        System.out.println(buffer.toString());
    }
}
