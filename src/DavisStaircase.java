import java.util.*;

/**
 * https://www.hackerrank.com/challenges/ctci-recursive-staircase
 * Task "Recursion: Davis' Staircase" from "Cracking the Coding Interview" contest.
 */
public class DavisStaircase {
    private static int counter = 0;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int s = scanner.nextInt();
        for (int i = 0; i < s; i++) {
            int nextVariant = scanner.nextInt();

            Node Node = new Node(Arrays.asList(nextVariant));
            List<Node> tree = new ArrayList<>(Arrays.asList(Node));
            List<Long> hashes = Arrays.asList(Node.getHash());
            while (tree.stream().anyMatch(e -> !e.isDone())) {
                Node thisNode = tree.stream().filter(e -> !e.isDone()).findFirst().get();
                List<Integer> thisSteps = thisNode.getSteps();
                for (int j = 0; j < thisSteps.size(); j++) {
                    if (thisSteps.get(j)>1){
                        List<Integer> newSteps = new ArrayList<>(thisSteps);
                        newSteps.set(j,newSteps.get(j)-1);
                    }
                }

                thisNode.setDone(true);
            }
        }
    }

}


class Node {
    private List<Integer> steps = new ArrayList<>();
    private boolean done = false;
    private long hash = 0;

    public void setDone(boolean done) {
        this.done = done;
    }

    public boolean isDone() {
        return done;
    }

    public List<Integer> getSteps() {
        return steps;
    }

    private static long calculateHash(List<Integer> list) {
        long result = 0;
        int powerOf10 = 1;
        for (int i = list.size() - 1; i >= 0; i--) {
            result += list.get(i) * powerOf10;
            powerOf10 *= 10;
        }

        return result;
    }

    public Node(List<Integer> steps) {
        this.steps = steps;
        int powerOf10 = 1;
        for (int i = steps.size() - 1; i >= 0; i--) {
            hash += steps.get(i) * powerOf10;
            powerOf10 *= 10;
        }
    }

    public long getHash() {
        return hash;
    }
}