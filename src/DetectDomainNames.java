import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class DetectDomainNames {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        s.nextLine();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i<n; i++){
            sb.append(s.nextLine()).append(System.lineSeparator());
        }

        Pattern p = Pattern.compile("(?<=\\:\\/\\/)([a-z0-9_\\-]+\\.)*[a-z0-9_\\-]+\\.[a-z]{2,}");
        Matcher m = p.matcher(sb.toString());
        List<String> result = new ArrayList<>();
        while(m.find()){
            String nextDomain = m.group();
            if (nextDomain.startsWith("www.") || nextDomain.startsWith("ww2.")) nextDomain = nextDomain.substring(4);
            result.add(nextDomain);
        }
        System.out.println(String.join(";",result.stream().sorted().distinct().collect(Collectors.toList())));
    }
}