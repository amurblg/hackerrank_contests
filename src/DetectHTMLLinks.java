import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * https://www.hackerrank.com/challenges/detect-html-links?utm_campaign=challenge-recommendation&utm_medium=email&utm_source=24-hour-campaign
 */
public class DetectHTMLLinks {
    public static void main(String[] args) {
        //Scanner scanner = new Scanner(System.in);
        Scanner scanner = new Scanner(new String("13\n" +
                "<div class=\"portal\" role=\"navigation\" id='p-navigation'>\n" +
                "<h3>Navigation</h3>\n" +
                "<div class=\"body\">\n" +
                "<ul>\n" +
                " <li id=\"n-mainpage-description\"><a href=\"/wiki/Main_Page\" title=\"Visit the main page [z]\" accesskey=\"z\">Main page</a></li>\n" +
                " <li id=\"n-contents\"><a href=\"/wiki/Portal:Contents\" title=\"Guides to browsing Wikipedia\">Contents</a></li>\n" +
                " <li id=\"n-featuredcontent\"><a href=\"/wiki/Portal:Featured_content\" title=\"Featured content  the best of Wikipedia\">Featured content</a></li>\n" +
                "<li id=\"n-currentevents\"><a href=\"/wiki/Portal:Current_events\" title=\"Find background information on current events\">Current events</a></li>\n" +
                "<li id=\"n-randompage\"><a href=\"/wiki/Special:Random\" title=\"Load a random article [x]\" accesskey=\"x\">Random article</a></li>\n" +
                "<li id=\"n-sitesupport\"><a href=\"//donate.wikimedia.org/wiki/Special:FundraiserRedirector?utm_source=donate&utm_medium=sidebar&utm_campaign=C13_en.wikipedia.org&uselang=en\" title=\"Support us\">Donate to Wikipedia</a></li>\n" +
                "</ul>\n" +
                "</div>\n" +
                "</div>"));
        int n = scanner.nextInt();
        scanner.nextLine();

        Pattern ahrefPattern = Pattern.compile("<a href=\\\".*?</a>");
        for (int i = 0; i < n; i++) {
            String line = scanner.nextLine();
            Matcher matcher1 = ahrefPattern.matcher(line);

        }
    }
}
