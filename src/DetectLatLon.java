/*
* https://www.hackerrank.com/challenges/detecting-valid-latitude-and-longitude
*
* */

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DetectLatLon {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
//        Scanner s = new Scanner(new String("14\n" +
//                "(-110, -6)\n" +
//                "(-110.158590, -6.158590)\n" +
//                "(-130, -219)\n" +
//                "(-130.581178, -219.581178)\n" +
//                "(-88, -241)\n" +
//                "(-88.344600, -241.344600)\n" +
//                "(-6, -165)\n" +
//                "(-6.871826, -165.871826)\n" +
//                "(-98, -40)\n" +
//                "(-98.122626, -40.122626)\n" +
//                "(-6, -172)\n" +
//                "(-6.377934, -172.377934)\n" +
//                "(-147, -266)\n" +
//                "(-147.357525, -266.357525)"));
        int n = s.nextInt();
        s.nextLine();

        Pattern p = Pattern.compile("^\\([\\+\\-]?[1-9]([0-9]+)?(\\.[0-9]+)?, [\\+\\-]?[1-9]([0-9]+)?(\\.[0-9]+)?\\)$");
        for (int i = 0; i < n; i++) {
            String line = s.nextLine();
            Matcher m = p.matcher(line);
            if (m.matches()) {
                String[] pair = m.group().split(", ");
                if (pair.length != 2) {
                    System.out.println("Invalid");
                    continue;
                }
                if (!checkDouble(pair[0].substring(1)) || !checkDouble(pair[1].substring(0, pair[1].length() - 1))) {
                    System.out.println("Invalid");
                    continue;
                }
                double lat = Double.parseDouble(pair[0].substring(1));
                double lon = Double.parseDouble(pair[1].substring(0, pair[1].length() - 1));

                if (lat < -90 || lat > 90 || lon < -180 || lon > 180) {
                    System.out.println("Invalid");
                    continue;
                }

                System.out.println("Valid");
            } else {
                System.out.println("Invalid");
            }
        }
    }

    private static boolean checkDouble(String d) {
        try {
            Double.parseDouble(d);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}