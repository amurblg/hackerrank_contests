/*
* https://www.hackerrank.com/challenges/determining-dna-health/problem
* */

import java.util.Scanner;

public class DeterminingDNAHealth {

    public static void main(String[] args) {
        //Scanner in = new Scanner(System.in);
        Scanner in = new Scanner(new String("6\n" +
                "a b c aa d b\n" +
                "1 2 3 4 5 6\n" +
                "3\n" +
                "1 5 caaab\n" +
                "0 4 xyz\n" +
                "2 4 bcdybc"));
        int n = in.nextInt();
        String[] genes = new String[n];
        for (int genes_i = 0; genes_i < n; genes_i++) {
            genes[genes_i] = in.next();
        }
        int[] health = new int[n];
        for (int health_i = 0; health_i < n; health_i++) {
            health[health_i] = in.nextInt();
        }
        int s = in.nextInt();

        int minHealth = Integer.MAX_VALUE;
        int maxHealth = 0;

        for (int a0 = 0; a0 < s; a0++) {
            int first = in.nextInt();
            int last = in.nextInt();
            String d = in.next();
            // your code goes here

        }
        System.out.println(minHealth + " " + maxHealth);
    }
}
