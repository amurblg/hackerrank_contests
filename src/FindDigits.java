import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/*
* https://www.hackerrank.com/challenges/find-digits
* */
public class FindDigits {
    public static void main(String[] args) {
        //Scanner s = new Scanner(System.in);
        Scanner s = new Scanner("2\n" +
                "12\n" +
                "1012");
        int t = Integer.parseInt(s.nextLine());
        for (int i = 0; i < t; i++) {
            long number = Long.parseLong(s.nextLine());
            int counter = 0;
            for (Integer digit : getDigits(number)) {
                if (digit != 0 && number % digit == 0) counter++;
            }
            System.out.println(counter);
        }
    }

    public static List<Integer> getDigits(long number) {
        List<Integer> result = new ArrayList<>();
        while (number > 0) {
            result.add((int) (number % 10));
            number /= 10;
        }

        return result;
    }
}
