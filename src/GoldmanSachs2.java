/*
* https://www.hackerrank.com/contests/gs-codesprint/challenges/buy-maximum-stocks
* */

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class GoldmanSachs2 {
    static class Pair implements Comparable<Pair> {
        private int weight;
        private int index;

        public Pair(int weight, int index) {
            this.weight = weight;
            this.index = index;
        }

        @Override
        public int compareTo(Pair o) {
            return weight - o.weight;
        }
    }

    static long buyMaximumProducts(int n, long k, int[] a) {
        // Complete this function
        ArrayList<Pair> queue = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            queue.add(new Pair(a[i], i + 1));
        }
        Collections.sort(queue);
        long totalAmount = 0;
        for (int i = 0; i < n; i++) {
            long thisPurchaseAmount = Math.min(k / queue.get(i).weight, queue.get(i).index);
            if (thisPurchaseAmount == 0) break;
            totalAmount += thisPurchaseAmount;
            k -= thisPurchaseAmount * queue.get(i).weight;
        }

        return totalAmount;
    }

    public static void main(String[] args) {
        //Scanner in = new Scanner(System.in);
        Scanner in = new Scanner(new String("3\n" +
                "10 7 19\n" +
                "45"));
        int n = in.nextInt();
        int[] arr = new int[n];
        for (int arr_i = 0; arr_i < n; arr_i++) {
            arr[arr_i] = in.nextInt();
        }
        long k = in.nextLong();
        long result = buyMaximumProducts(n, k, arr);
        System.out.println(result);
        in.close();
    }
}
