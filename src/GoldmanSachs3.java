/*
* https://www.hackerrank.com/contests/gs-codesprint/challenges/trader-profit
* */

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class GoldmanSachs3 {
    static class Pair implements Comparable<Pair> {
        private int price;
        private int day;

        public Pair(int price, int day) {
            this.price = price;
            this.day = day;
        }

        @Override
        public String toString() {
            return "Pair{" +
                    "price=" + price +
                    ", day=" + day +
                    '}';
        }

        @Override
        public int compareTo(Pair o) {
            return price - o.price;
        }
    }

    static int traderProfit(int k, int n, int[] A) {
        // Complete this function
        ArrayList<Pair> list = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            list.add(new Pair(A[i], i + 1));
        }
        LinkedList<Pair> sorted = new LinkedList<>(list);
        Collections.sort(sorted);
        System.out.println(list);
        System.out.println(sorted);

        return 0;
    }

    public static void main(String[] args) {
        //Scanner in = new Scanner(System.in);
        Scanner in = new Scanner(new String("1\n1\n5\n12 5 10 7 17"));
        int q = in.nextInt();
        for (int a0 = 0; a0 < q; a0++) {
            int k = in.nextInt();
            int n = in.nextInt();
            int[] arr = new int[n];
            for (int arr_i = 0; arr_i < n; arr_i++) {
                arr[arr_i] = in.nextInt();
            }
            int result = traderProfit(k, n, arr);
            System.out.println(result);
        }
        in.close();
    }
}
