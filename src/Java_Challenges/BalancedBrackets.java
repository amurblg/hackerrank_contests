package Java_Challenges;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Stack;

/**
 * https://www.hackerrank.com/challenges/balanced-brackets?h_r=next-challenge&h_v=zen
 */
public class BalancedBrackets {
    public static void main(String[] args) throws IOException {
//        long start = System.currentTimeMillis();
        run();
//        System.out.println("Done in " + (System.currentTimeMillis() - start) + " ms");
    }

    private static void run() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        BufferedReader reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(("3\n" +
//                "(){[()]}()\n" +
//                "{[(])}\n" +
//                "{{[[(())]]}}").getBytes())));

        int n = Integer.parseInt(reader.readLine());
        for (int i = 0; i < n; i++) {
            String nextLine = reader.readLine().trim();
            char[] symbols = nextLine.toCharArray();

            boolean isBalanced = true;
            Stack<Character> stack = new Stack<>();
            for (int j = 0; j < symbols.length; j++) {
                switch (symbols[j]) {
                    case '}':
                        if (stack.peek().compareTo('{') != 0) {
                            isBalanced = false;
                            break;
                        } else stack.pop();
                        break;
                    case ']':
                        if (stack.peek().compareTo('[') != 0) {
                            isBalanced = false;
                            break;
                        } else stack.pop();
                        break;
                    case ')':
                        if (stack.peek().compareTo('(') != 0) {
                            isBalanced = false;
                            break;
                        } else stack.pop();
                        break;
                    case '{':
                    case '[':
                    case '(':
                        stack.push(symbols[j]);
                        break;
                    default:
                        break;
                }
            }
            System.out.println(isBalanced ? "YES" : "NO");
/*
            while (true) {
                int prevLen = nextLine.length();
                nextLine = nextLine.replaceAll("^\\{(.*)\\}$", "$1");
                nextLine = nextLine.replaceAll("^\\[(.*)\\]$", "$1");
                nextLine = nextLine.replaceAll("^\\((.*)\\)$", "$1");
                nextLine = nextLine.replaceAll("^(\\(\\))|(\\{\\})|(\\[\\])(.*)$", "$4");
                nextLine = nextLine.replaceAll("^(.*)(\\(\\))|(\\{\\})|(\\[\\])$", "$1");
                if (nextLine.length() == prevLen) break;
            }
*/
        }

        reader.close();
    }
}
