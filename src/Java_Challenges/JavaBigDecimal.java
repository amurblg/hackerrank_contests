/*
*
* https://www.hackerrank.com/challenges/java-bigdecimal
*
* */

package Java_Challenges;

import java.math.BigDecimal;
import java.util.*;

class JavaBigDecimal {

    public static void main(String[] args) {
        //Input
        Scanner sc = new Scanner(System.in);
/* Test data example
        Scanner sc = new Scanner(new String("9\n" +
                "-100\n" +
                "50\n" +
                "0\n" +
                "56.6\n" +
                "90\n" +
                "0.12\n" +
                ".12\n" +
                "02.34\n" +
                "000.000"));
*/
        int n = sc.nextInt();
        String[] s = new String[n + 2];
        for (int i = 0; i < n; i++) {
            s[i] = sc.next();
        }
        sc.close();
        Map<Integer, BigDecimal> mapBD = new LinkedHashMap<>();
        Map<Integer, String> mapStr = new HashMap<>();
        List<String> result = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            mapStr.put(i, s[i]);
            mapBD.put(i, new BigDecimal(mapStr.get(i)));
        }
        mapBD.entrySet().stream()
                .sorted((a, b) -> b.getValue().compareTo(a.getValue()))
                .forEachOrdered(e -> result.add(mapStr.get(e.getKey())));

        for (int i = 0; i < n; i++) {
            s[i] = result.get(i);
        }
        //Output
        for (int i = 0; i < n; i++) {
            System.out.println(s[i]);
        }
    }
}