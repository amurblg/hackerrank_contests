package Java_Challenges;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * https://www.hackerrank.com/challenges/maximum-element
 * See also maxElement_testData.txt and maxElement_expectedOutput.txt in testData package.
 */
public class MaximumInStack {
    public static void main(String[] args) throws IOException {
        long start = System.currentTimeMillis();
        run();
        System.out.println("Done in " + (System.currentTimeMillis() - start) + " ms");
    }

    private static void run() throws IOException {
        //BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader reader = new BufferedReader(new FileReader(System.getProperty("user.dir") + System.getProperty("file.separator") + "src" + System.getProperty("file.separator") + "testData" + System.getProperty("file.separator") + "maxElement_test3_data.txt"));
//        BufferedReader reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(("10\n" +
//                "1 97\n" +
//                "2\n" +
//                "1 20\n" +
//                "2\n" +
//                "1 26\n" +
//                "1 20\n" +
//                "2\n" +
//                "3\n" +
//                "1 91\n" +
//                "3").getBytes())));
        int n = Integer.parseInt(reader.readLine());

        int[][] data = new int[2][n];
        int currentElement = 0;
        int max = 0;
        for (int i = 0; i < n; i++) {
            String[] nextLine = reader.readLine().split(" ");
            switch (Integer.parseInt(nextLine[0])) {
                case 1:
                    data[0][currentElement] = Integer.parseInt(nextLine[1]);
                    if (data[0][currentElement] > max) {
                        max = data[0][currentElement];
                        data[1][currentElement] = max;
                    } else data[1][currentElement] = (currentElement == 0 ? 0 : data[1][currentElement - 1]);
                    currentElement++;
                    break;
                case 2:
                    currentElement--;
                    if (currentElement == 0) max = 0;
                    else max = data[1][currentElement - 1];
                    break;
                case 3:
                    System.out.println(max);
                    break;
                default:
                    break;
            }
        }

        reader.close();
    }
}
