/*
* https://www.hackerrank.com/contests/moodys-analytics-fall-university-codesprint/challenges/cost-balancing
* */

package MoodysAnalyticsFallUC;

import java.util.Scanner;

public class CostBalancing {
    public static void main(String[] args) {
        //Scanner s = new Scanner(System.in);
        Scanner s = new Scanner("7 5\n" +
                "1 200\n" +
                "2 200\n" +
                "3 100\n" +
                "4 10\n" +
                "5 54\n" +
                "5 54\n" +
                "3 100");
        int n = s.nextInt();
        int m = s.nextInt();

        int[][] transactions = new int[n][2];
        int[] friends = new int[m];

        int total = 0;
        for (int i = 0; i < n; i++) {
            transactions[i][0] = s.nextInt();
            transactions[i][1] = s.nextInt();
            total += transactions[i][1];
            friends[transactions[i][0] - 1] += transactions[i][1];
        }

        int avg = total / m;
        System.out.println("1 " + String.valueOf(friends[0] - (total - avg * (m - 1))));
        for (int i = 1; i < m; i++) {
            System.out.println(i + 1 + " " + String.valueOf(friends[i] - avg));
        }
    }
}
