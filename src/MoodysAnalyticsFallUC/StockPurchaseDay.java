/*
* https://www.hackerrank.com/contests/moodys-analytics-fall-university-codesprint/challenges/stock-purchase-day
* */

package MoodysAnalyticsFallUC;

import java.util.Arrays;
import java.util.Scanner;

public class StockPurchaseDay {
    public static void main(String[] args) {
        //Scanner s = new Scanner(System.in);
        Scanner s = new Scanner("8\n" +
                "4 2 2 3 3 4 6 5\n" +
                "5 \n" +
                "2\n" +
                "1\n" +
                "3\n" +
                "4\n" +
                "5");

        int n = s.nextInt();
        DayPrice[] dayPrice = new DayPrice[n];
        for (int i = 0; i < n; i++) {
            dayPrice[i] = new DayPrice(Integer.parseInt(s.next()), i + 1);
        }
        Arrays.sort(dayPrice);

        int q = s.nextInt();
        int[] custProposals = new int[q];
        for (int i = 0; i < q; i++) {
            custProposals[i] = Integer.parseInt(s.next());
            int pos = Arrays.binarySearch(dayPrice, new DayPrice(custProposals[i], 0));
            if (pos != -1) {
                if (pos < 0) pos = Math.abs(pos + 1);
                while (pos + 1 < dayPrice.length && dayPrice[pos + 1].price <= custProposals[i]) {
                    if (pos + 1 >= dayPrice.length) break;
                    pos++;
                }
                pos = dayPrice[pos].day;
            }
            System.out.println(pos);
        }
    }

    static class DayPrice implements Comparable<DayPrice> {
        int price;
        int day;

        public DayPrice(int price, int day) {
            this.price = price;
            this.day = day;
        }

        @Override
        public int compareTo(DayPrice o) {
            return price - o.price;
        }
    }
}
