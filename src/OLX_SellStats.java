import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.function.Predicate;

public class OLX_SellStats {
    static List<Purchase> purchases = new ArrayList<>();

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner s = new Scanner(System.in);
        int t = s.nextInt();
        s.nextLine();

        for (int i = 0; i < t; i++) {
            String nextLine = s.nextLine();
            if (nextLine.toLowerCase().startsWith("s")) {
                purchases.add(new Purchase(nextLine.substring(2)));
            } else if (nextLine.toLowerCase().startsWith("q")) {
                Predicate<Purchase> queryFilter = getFilterPredicate(nextLine.substring(2));
                if (queryFilter != null) System.out.println(purchases.stream().filter(queryFilter).count());
                else System.out.println(0);
            }
        }
    }

    public static Predicate<Purchase> getFilterPredicate(String unparsedQuery) {
        if (purchases.size() == 0) return null;

        String[] tokens = unparsedQuery.split(" +");
        int dayStart;
        int dayEnd = 0;
        if (tokens[0].contains(".")) {
            String[] tmp = tokens[0].split("\\.");
            dayStart = Integer.parseInt(tmp[0]);
            dayEnd = Integer.parseInt(tmp[1]);
        } else dayStart = Integer.parseInt(tokens[0]);

        int productId;
        int catId = 0;
        if (tokens[1].contains(".")) {
            productId = Integer.parseInt(tokens[1].split("\\.")[0]);
            catId = Integer.parseInt(tokens[1].split("\\.")[1]);
        } else productId = Integer.parseInt(tokens[1]);

        int stateId;
        int regionId = 0;
        if (tokens[2].contains(".")) {
            String[] tmp = tokens[2].split("\\.");
            stateId = Integer.parseInt(tmp[0]);
            regionId = Integer.parseInt(tmp[1]);
        } else stateId = Integer.parseInt(tokens[2]);

        final int pId = productId;
        final int cId = catId;
        final int dStart = dayStart;
        final int dEnd = dayEnd;
        final int sId = stateId;
        final int rId = regionId;

        Predicate<Purchase> result = null;
        if (pId != -1) result = e -> e.getProductId() == pId;
        if (dEnd != 0) {
            result = (result != null ? result.and(e -> e.getDay() <= dEnd && e.getDay() >= dStart) : e -> e.getDay() <= dEnd && e.getDay() >= dStart);
        } else result = (result != null ? result.and(e -> e.getDay() == dStart) : e -> e.getDay() == dStart);
        if (cId != 0) result = result.and(e -> e.getCatId() == cId);
        if (sId != -1) {
            result = result.and(e -> e.getStateId() == sId);
            if (rId != 0) result = result.and(e -> e.getRegionId() == rId);
        }

        return result;
    }
}

class Purchase {
    private int productId;
    private int catId;
    private int stateId;
    private int regionId;
    private int day;

    public int getProductId() {
        return productId;
    }

    public int getCatId() {
        return catId;
    }

    public int getStateId() {
        return stateId;
    }

    public int getRegionId() {
        return regionId;
    }

    public int getDay() {
        return day;
    }

    public Purchase(String unparsedString) {
        String[] tokens = unparsedString.split(" +");
        int day = Integer.parseInt(tokens[0]);
        int productId = 0;
        int catId = 0;
        if (tokens[1].contains(".")) {
            String[] tmp = tokens[1].split("\\.");
            productId = Integer.parseInt(tmp[0]);
            catId = Integer.parseInt(tmp[1]);
        } else productId = Integer.parseInt(tokens[1]);

        int stateId = 0;
        int regionId = 0;
        if (tokens[2].contains(".")) {
            String[] tmp = tokens[2].split("\\.");
            stateId = Integer.parseInt(tmp[0]);
            regionId = Integer.parseInt(tmp[1]);
        } else stateId = Integer.parseInt(tokens[2]);

        this.day = day;
        this.catId = catId;
        this.productId = productId;
        this.regionId = regionId;
        this.stateId = stateId;
    }
}
