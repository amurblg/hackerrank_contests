import java.util.*;

/*
* https://www.hackerrank.com/contests/openbracket-2017/challenges/finding-subsequence/problem*
 */

public class OpenBracketDelaware_FindSubsequence {

    static String solve(String s, int k) {
        // Complete this function

        char[] letters = s.toCharArray();
        int[] freqs = new int[letters.length];
        boolean[] valid = new boolean[letters.length];
        int[] priority = new int[letters.length];

        for (int i = 0; i < freqs.length; i++) {
            char letter = letters[i];
            boolean alreadyCounted = false;
            for (int j = 0; j < i; j++) {
                if (letters[j] == letter) {
                    alreadyCounted = true;
                    freqs[i] = freqs[j];
                    break;
                }
            }
            if (!alreadyCounted) freqs[i] = countOccurencies(letters, letter);
            valid[i] = (freqs[i] < k ? false : true);
        }

        Map<Character, Integer> lettersCounter = new HashMap<>();
        for (int i = letters.length - 1; i >= 0; i--) {
            if (!valid[i]) {
                continue;
            }
            if (lettersCounter.containsKey(letters[i]))
                lettersCounter.replace(letters[i], lettersCounter.get(letters[i]) + 1);
            else
                lettersCounter.put(letters[i], 1);
        }

        char[] sorted = Arrays.copyOf(letters, letters.length);
        Arrays.sort(sorted);
        List<Character> uniqLetters = new ArrayList<>();
        for (int i = 0; i < sorted.length; i++) {
            if (uniqLetters.contains(sorted[i])) continue;
            uniqLetters.add(sorted[i]);
        }
        for (int i = 0; i < letters.length; i++) {
            for (int j = 0; j < uniqLetters.size(); j++) {
                if (letters[i] == uniqLetters.get(j)) {
                    priority[i] = j;
                    break;
                }
            }
        }

        StringBuilder sb = new StringBuilder();
        int currPos = -1;
        int maxValidPos = -1;
        while (currPos < letters.length) {
            maxValidPos = getNextMaxValidLetterPosition(letters, valid, priority, currPos);
            if (maxValidPos >= letters.length) break;
            sb.append(String.valueOf(letters[maxValidPos]));
            currPos = maxValidPos + 1;
        }

        letters = sb.toString().toCharArray();
        lettersCounter = new HashMap<>();
        for (int i = 0; i < letters.length; i++) {
            if (lettersCounter.containsKey(letters[i]))
                lettersCounter.replace(letters[i], lettersCounter.get(letters[i]) + 1);
            else lettersCounter.put(letters[i], 1);
        }

        sb = new StringBuilder();
        for (int i = 0; i < letters.length; i++) {
            if (lettersCounter.get(letters[i]) < k) letters[i] = 0;
            if (letters[i] != 0) sb.append(String.valueOf(letters[i]));
        }
/*
         1
        [h][a][c][k][e][r][r][a][n][k]
         1  2  1  2  1  2  2  2  1  2 freqs
         +  +  +  +  +  +  +  +  +  + valid
         3  0  1  4  2  6  6  0  5  4 priority

         2
        [b][a][n][a][n][a] letters
         1  3  2  3  2  3  freqs
         -  +  +  +  +  +  valid
         1  0  2  0  2  0  priority

         3
        [b][a][n][a][n][a] letters
         1  3  2  3  2  3  freqs
         -  +  -  +  -  +  valid
         1  0  2  0  2  0  priority
*/

        return sb.toString();
    }

    private static int getNextMaxValidLetterPosition(char[] letters, boolean[] valid, int[] priority, int startFrom) {
        boolean getCorrectResult = false;
        if (startFrom == -1) {
            for (int i = 0; i < valid.length; i++) {
                if (valid[i]) {
                    startFrom = i;
                    getCorrectResult = true;
                    break;
                }
            }
        }
        if (!valid[startFrom]) {
            for (int i = startFrom; i < valid.length; i++) {
                if (valid[i]) {
                    startFrom = i;
                    getCorrectResult = true;
                    break;
                }
            }
        } else getCorrectResult = true;

        if (!getCorrectResult) return letters.length;

        int currMax = startFrom;
        for (int i = startFrom; i < letters.length; i++) {
            if (valid[i] && priority[i] > priority[currMax]) currMax = i;
        }

        return currMax;
    }

    private static int countOccurencies(char[] array, char letter) {
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == letter) count++;
        }

        return count;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
//        Scanner in = new Scanner(new String("banana\n" +
//                "2"));
//        Scanner in = new Scanner(new String("hackerrank\n" +
//                "1"));
//        Scanner in = new Scanner(new String("banana\n" +
//                "3"));
        String s = in.next();
        int k = in.nextInt();
        String result = solve(s, k);
        System.out.println(result);
        in.close();
    }
}
