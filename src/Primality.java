import java.util.Scanner;
import java.util.stream.LongStream;

/**
 * https://www.hackerrank.com/challenges/ctci-big-o
 * "Cracking the Coding Interview" contest; task "Time Complexity: Primality"
 * difficulty: medium; max score: 30
 */
public class Primality {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            int nextCandidate = scanner.nextInt();
            if (nextCandidate < 2) System.out.println("Not prime");
            else System.out.println(LongStream.rangeClosed(2, (long) Math.sqrt(nextCandidate)).anyMatch(j -> nextCandidate % j == 0) ? "Not prime" : "Prime");
        }
    }
}
