/*
* https://www.hackerrank.com/challenges/sherlock-and-array
* */

import java.util.Arrays;
import java.util.Scanner;

public class SherlockAndArray {

    static String solve(int[] a) {
        // Complete this function
        int len = a.length;
        int[] ltor = new int[len];
        int[] rtol = new int[len];
        ltor[0] = 0;
        rtol[len - 1] = 0;

        for (int i = 1; i < len; i++) {
            ltor[i] = ltor[i - 1] + a[i - 1];
            rtol[len - i - 1] = rtol[len - i] + a[len - i];
        }
//        System.out.println(Arrays.toString(a));
//        System.out.println(Arrays.toString(ltor));
//        System.out.println(Arrays.toString(rtol));
        for (int i = 0; i < len; i++) {
            if (ltor[i] == rtol[i]) return "YES";
        }
        return "NO";
    }

    public static void main(String[] args) {
        //Scanner in = new Scanner(System.in);
        Scanner in = new Scanner("2\n" +
                "3\n" +
                "1 2 3\n" +
                "4\n" +
                "1 2 3 3");
        int T = in.nextInt();
        for (int a0 = 0; a0 < T; a0++) {
            int n = in.nextInt();
            int[] a = new int[n];
            for (int a_i = 0; a_i < n; a_i++) {
                a[a_i] = in.nextInt();
            }
            String result = solve(a);
            System.out.println(result);
        }
    }
}
