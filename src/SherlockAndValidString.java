/*
* https://www.hackerrank.com/challenges/sherlock-and-valid-string?h_r=next-challenge&h_v=zen
* */

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;

public class SherlockAndValidString {
    static String isValid(String s) {
        // Complete this function
        if (s.length() == 0) return "NO";
        Map<Character, Long> freqs = s.chars()//Какой символ сколько раз встречается
                .mapToObj(e -> (char) e)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        System.out.println(freqs);//На примере "aabbcd": {a=2, b=2, c=1, d=1}

        Map<Long, Integer> freqs2 = new HashMap<>();//Ключ - частота встречаемости символа, значение - сколько символов встречается такое количество раз
        freqs.forEach((k, v) -> freqs2.compute(v, (k2, v2) -> v2 == null ? 1 : ++v2));
        System.out.println(freqs2);//На примере "aabbcd": {1=2, 2=2}
        if (freqs2.size() > 2) return "NO";
        if (freqs2.size() == 1) return "YES";

        long minKey = Long.MAX_VALUE;
        long minValue = Long.MAX_VALUE;
        long maxKey = 0;
        long maxValue = 0;
        for (Long f : freqs2.keySet()) {
            if (f < minKey) {
                minKey = f;
                minValue = freqs2.get(f);
            }
            if (f > maxKey) {
                maxKey = f;
                maxValue = freqs2.get(f);
            }
        }

        if (minKey == 1 && minValue == 1) return "YES";//Ситуации вида "aaabbbc"
        if (maxKey - minKey == 1 && maxValue == 1) return "YES";//Ситуации вида "aaaabbbbccccc"
        return "NO";
    }

    public static void main(String[] args) {
        //Scanner in = new Scanner(System.in);
        Scanner in = new Scanner(new String("jtqgugmcsxvdwidtcyqpogkdifapuloqykjfxruvfrshcehekoiwbpbrprahwvhliglyxynjotbaswnnnmxbkmcftvsdqajemeul"));
        String s = in.next();
        String result = isValid(s);
        System.out.println(result);
    }
}
