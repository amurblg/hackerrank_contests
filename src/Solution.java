import java.util.*;

class NNode {
    NNode left, right;
    int data;

    NNode(int data) {
        this.data = data;
        left = right = null;
    }
}

public class Solution {
    static void levelOrder(NNode root) {
        //Write your code here
        
    }

    public static NNode insert(NNode root, int data) {
        if (root == null) {
            return new NNode(data);
        } else {
            NNode cur;
            if (data <= root.data) {
                cur = insert(root.left, data);
                root.left = cur;
            } else {
                cur = insert(root.right, data);
                root.right = cur;
            }
            return root;
        }
    }

    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        int T = sc.nextInt();
        NNode root = null;
        while (T-- > 0) {
            int data = sc.nextInt();
            root = insert(root, data);
        }
        levelOrder(root);
    }
}
