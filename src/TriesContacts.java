/**
 * https://www.hackerrank.com/challenges/ctci-contacts
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.*;

public class TriesContacts {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(System.in);

/*
        System.out.println(System.getProperty("user.dir"));
        Scanner scanner = new Scanner(new File(System.getProperty("user.dir") + "\\src\\testData\\triesContacts50k1.txt"));
        FileOutputStream fos = new FileOutputStream(System.getProperty("user.dir") + "\\src\\testData\\triesContacts50k1_output.txt");
        PrintStream ps = new PrintStream(fos);
        System.setOut(ps);
*/

        int n = scanner.nextInt();
        scanner.nextLine();

        Trie rootNode = new Trie();

        for (int i = 0; i < n; i++) {
            String[] nextLine = scanner.nextLine().split(" ");

            if ("add".equals(nextLine[0])) {
                char[] wordChars = nextLine[1].toCharArray();
                Trie nextLetterTrie = rootNode;
                for (int j = 0; j < wordChars.length; j++) {
                    if (!nextLetterTrie.getChildren().containsKey(wordChars[j])) {
                        Trie tmp = new Trie();
                        nextLetterTrie.getChildren().put(wordChars[j], tmp);
                        nextLetterTrie = tmp;
                    } else {
                        nextLetterTrie = nextLetterTrie.getChildren().get(wordChars[j]);
                        nextLetterTrie.incrementNumOfWords();
                    }
                }
                if (!nextLetterTrie.getChildren().containsKey('*')) {
                    Trie tmp = new Trie(true);
                    nextLetterTrie.getChildren().put('*', tmp);
                }
            } else if ("find".equals(nextLine[0])) {
                char[] wordChars = nextLine[1].toCharArray();
                if (!rootNode.getChildren().containsKey(wordChars[0]))
                    System.out.println(0);
                else {
                    Trie nextLetterTrie = rootNode.getChildren().get(wordChars[0]);
                    boolean noSuchWord = false;
                    for (int j = 1; j < wordChars.length; j++) {
                        if (!nextLetterTrie.getChildren().containsKey(wordChars[j])) {
                            System.out.println(0);
                            noSuchWord = true;
                            break;
                        } else {
                            nextLetterTrie = nextLetterTrie.getChildren().get(wordChars[j]);
                        }
                    }
                    if (!noSuchWord) {
                        System.out.println(nextLetterTrie.getNumOfWordsStarted());
                    }
                }
            }
        }
    }
}

class Trie {
    private Map<Character, Trie> children = new HashMap<>();
    private boolean isEndOfWord = false;
    private int numOfWordsStarted = 1;

    public Trie() {
    }

    public Trie(boolean isEndOfWord) {
        this();
        this.isEndOfWord = isEndOfWord;
    }

    public int getNumOfWordsStarted() {
        return numOfWordsStarted;
    }

    public void incrementNumOfWords() {
        numOfWordsStarted++;
    }

    public Map<Character, Trie> getChildren() {
        return children;
    }

    public boolean isEndOfWord() {
        return isEndOfWord;
    }
}