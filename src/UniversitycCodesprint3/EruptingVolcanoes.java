package UniversitycCodesprint3;

import java.util.Scanner;

public class EruptingVolcanoes {

    public static void main(String[] args) {
//        Scanner in = new Scanner(System.in);
        Scanner in = new Scanner("10\n" +
                "2\n" +
                "3 3 3 \n" +
                "7 7 4");
        int n = in.nextInt();
        int[][] matrix = new int[n][n];

        int m = in.nextInt();
        for (int a0 = 0; a0 < m; a0++) {
            int x = in.nextInt();
            int y = in.nextInt();
            int w = in.nextInt();
            // Write Your Code Here
            for (int delta = w; delta > 0; delta--) {
                for (int dx = x - delta + 1; dx < x + delta; dx++) {
                    for (int dy = y - delta + 1; dy < y + delta; dy++) {
                        if ((dx >= 0 && dx < n) && (dy >= 0 && dy < n)) matrix[dx][dy]++;
                    }
                }
            }
        }
        in.close();
        int max = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
//                System.out.print(matrix[i][j] + " ");
                if (matrix[i][j] > max) max = matrix[i][j];
            }
//            System.out.println();
        }
        System.out.println(max);
    }
}
