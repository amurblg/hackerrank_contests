package UniversitycCodesprint3;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Snake {

    public static void main(String[] args) {
        //Scanner in = new Scanner(System.in);
        Scanner in = new Scanner("4\n" +
                "n\n" +
                "3 3");
        int n = in.nextInt();
        char d = in.next().charAt(0);
        int x = in.nextInt();
        int y = in.nextInt();
        // Write Your Code Here
        in.close();

        Map<Character, DIRECTIONS[]> priorities = new HashMap<>();
        priorities.put('n', new DIRECTIONS[]{DIRECTIONS.UP, DIRECTIONS.LEFT, DIRECTIONS.RIGHT, DIRECTIONS.DOWN});
        priorities.put('s', new DIRECTIONS[]{DIRECTIONS.DOWN, DIRECTIONS.LEFT, DIRECTIONS.RIGHT, DIRECTIONS.UP});
        priorities.put('e', new DIRECTIONS[]{DIRECTIONS.RIGHT, DIRECTIONS.UP, DIRECTIONS.DOWN, DIRECTIONS.LEFT});
        priorities.put('w', new DIRECTIONS[]{DIRECTIONS.LEFT, DIRECTIONS.UP, DIRECTIONS.DOWN, DIRECTIONS.RIGHT});

        boolean[][] visitedCells = new boolean[n][n];
        int[][] path = new int[n][n];
        path[x][y] = 1;
        visitedCells[x][y] = true;

        for (int i = 1; i < n * n; i++) {
            int possibleX = x;
            int possibleY = y;
            for (int possibleMove = 0; possibleMove < 4; possibleMove++) {
                switch (priorities.get(d)[possibleMove]) {
                    case UP:
                        possibleX = x - 1;
                        possibleY = y;
                        break;
                    case RIGHT:
                        possibleY = y + 1;
                        possibleX = x;
                        break;
                    case DOWN:
                        possibleX = x + 1;
                        possibleY = y;
                        break;
                    case LEFT:
                        possibleY = y - 1;
                        possibleX = x;
                        break;
                }

                if ((possibleX >= 0 && possibleX < n) && (possibleY >= 0 && possibleY < n) && !visitedCells[possibleX][possibleY]) {
                    x = possibleX;
                    y = possibleY;
                    path[x][y] = i + 1;
                    visitedCells[x][y] = true;
                    break;
                } else continue;
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(path[i][j] + " ");
            }
            System.out.println();
        }
    }
}

enum DIRECTIONS {
    UP, RIGHT, DOWN, LEFT
}