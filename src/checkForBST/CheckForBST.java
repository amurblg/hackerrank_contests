/*
* https://www.hackerrank.com/challenges/ctci-is-binary-search-tree/problem
* */

package checkForBST;

import java.util.HashSet;
import java.util.Set;

public class CheckForBST {
    static Set<Integer> uniques = new HashSet<>();

    static boolean checkBST(Node root) {
        if (uniques.contains(root.data)) return false;
        uniques.add(root.data);
        if (!noBiggerChildren(root.data, root.left)) return false;
        if (!noSmallerChildren(root.data, root.right)) return false;


        if (root.left == null && root.right == null) return true;
        if (root.left == null) return (root.data < root.right.data) && checkBST(root.right);
        if (root.right == null) return (root.data > root.left.data) && checkBST(root.left);
        if (root.left.data < root.right.data)
            return (root.data < root.right.data) && (root.data > root.left.data) && checkBST(root.left) && checkBST(root.right);

        return false;
    }

    //Рекурсивная проверка того, что в данном поддереве нет узлов со значением больше заданного. Применяем к левому дочернему узлу.
    static boolean noBiggerChildren(int data, Node root) {
        if (root == null) return true;

        if (root.left == null && root.right == null) return true;
        if (root.left == null) return (data > root.right.data) && noBiggerChildren(data, root.right);
        if (root.right == null) return (data > root.left.data) && noBiggerChildren(data, root.left);
        return (data > root.left.data && data > root.right.data)
                && noBiggerChildren(data, root.left)
                && noBiggerChildren(data, root.right);
    }

    //Рекурсивная проверка того, что в данном поддереве нет узлов со значением меньше заданного. Применяем к правому дочернему узлу.
    static boolean noSmallerChildren(int data, Node root) {
        if (root == null) return true;

        if (root.left == null && root.right == null) return true;
        if (root.left == null) return (data < root.right.data) && noSmallerChildren(data, root.right);
        if (root.right == null) return (data < root.left.data) && noSmallerChildren(data, root.left);
        return (data < root.left.data) && (data < root.right.data)
                && noSmallerChildren(data, root.left)
                && noSmallerChildren(data, root.right);
    }

    public static void main(String[] args) {

    }

}

class Node {
    int data;
    Node left;
    Node right;
}
