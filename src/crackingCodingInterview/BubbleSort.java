package crackingCodingInterview;

import java.util.Scanner;

/**
 * https://www.hackerrank.com/challenges/ctci-bubble-sort
 */
public class BubbleSort {
    public static void main(String[] args) {
        //Scanner scanner = new Scanner(System.in);
        Scanner scanner = new Scanner(new String("3\n" +
                "3 2 1"));
        int n = scanner.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = scanner.nextInt();
        }

        System.out.printf("Array is sorted in %d swaps.\n", numOfSwapsBubbleSort(array));
        System.out.printf("First Element: %d\n", array[0]);
        System.out.printf("Last Element: %d\n", array[n-1]);
    }

    public static int numOfSwapsBubbleSort(int[] array) {
        int result = 0;
        int n = array.length;

        for (int i = 0; i < n; i++) {
            int thisRoundSwaps = 0;
            for (int j = 0; j < n - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int t = array[j + 1];
                    array[j + 1] = array[j];
                    array[j] = t;
                    thisRoundSwaps++;
                }
            }
            if (thisRoundSwaps == 0) break;
            result += thisRoundSwaps;
        }

        return result;
    }
}
