package crackingCodingInterview;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * https://www.hackerrank.com/challenges/ctci-connected-cell-in-a-grid
 */
public class ConnectedCellInAGrid {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
//        Scanner in = new Scanner("4\n" +
//                "4\n" +
//                "1 1 0 0\n" +
//                "0 1 0 0\n" +
//                "0 0 0 1\n" +
//                "1 1 1 1");
        int n = in.nextInt();
        int m = in.nextInt();
        in.nextLine();
        Item[][] matrix = new Item[n][m];
        for (int grid_i = 0; grid_i < n; grid_i++) {
            String[] line = in.nextLine().split(" ");
            for (int grid_j = 0; grid_j < m; grid_j++) {
                matrix[grid_i][grid_j] = new Item(Integer.parseInt(line[grid_j]) == 1 ? true : false, grid_i, grid_j);
            }
        }

        int max = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (matrix[i][j].isValue()) {
                    if (!matrix[i][j].isChecked()) {
                        int regionSize = 1 + recursiveCountRegionSize(matrix, matrix[i][j]);
                        if (max < regionSize) max = regionSize;
                    }
                } else matrix[i][j].setChecked(true);
            }
        }

        System.out.println(max);
    }

    private static int recursiveCountRegionSize(Item[][] matrix, Item nextItem) {
        nextItem.setChecked(true);
        Set<Item> neighbors = getNeighbors(matrix, nextItem);
        int result = neighbors.size();
        for (Item item : neighbors) {
            item.setChecked(true);
        }
        for (Item item : neighbors) {
            result += recursiveCountRegionSize(matrix, item);
        }

        return result;
    }

    private static Set<Item> getNeighbors(Item[][] matrix, Item item) {
        Set<Item> result = new HashSet<>();
        for (int i = item.getRow() - 1; i <= item.getRow() + 1; i++) {
            for (int j = item.getColumn() - 1; j <= item.getColumn() + 1; j++) {
                if (i >= 0 && i < matrix.length && j >= 0 && j < matrix[0].length) {
                    if (matrix[i][j].isValue() && !matrix[i][j].isChecked() && !(item.getColumn() == j && item.getRow() == i))
                        result.add(matrix[i][j]);
                }
            }
        }

        return result;
    }
}

class Item {
    private boolean value;
    private boolean checked = false;
    private int row;
    private int column;

    public Item(boolean value, int row, int column) {
        this.value = value;
        this.row = row;
        this.column = column;
    }

    @Override
    public String toString() {
        return String.format("{[%d %d]: %s (%s)}", row, column, Boolean.toString(value), checked ? "checked" : "unchecked");
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public boolean isValue() {
        return value;
    }

    public boolean isChecked() {

        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}