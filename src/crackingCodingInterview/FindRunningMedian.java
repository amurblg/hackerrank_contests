package crackingCodingInterview;

import java.util.*;

public class FindRunningMedian {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        PriorityQueue<Integer> leftPart = new PriorityQueue<>((a, b) -> b - a);
        PriorityQueue<Integer> rightPart = new PriorityQueue<>();
        int middle = scanner.nextInt();
        System.out.printf("%.1f\n", (double) middle);

        for (int i = 2; i <= n; i++) {
            int next = scanner.nextInt();
            if (i % 2 == 0) {
                if (next > middle) {
                    leftPart.add(middle);
                    rightPart.add(next);
                } else {
                    rightPart.add(middle);
                    leftPart.add(next);
                }
                if (rightPart.peek() < leftPart.peek()) {
                    int tmp = leftPart.poll();
                    leftPart.add(rightPart.poll());
                    rightPart.add(tmp);
                }
                System.out.printf("%.1f\n", (double) (leftPart.peek() + rightPart.peek()) / 2.0);
                //System.out.println(leftPart + " " + rightPart);
            } else {
                if (next >= leftPart.peek() && next <= rightPart.peek()) {
                    middle = next;
                } else if (next > rightPart.peek()) {
                    middle = rightPart.poll();
                    rightPart.add(next);
                } else if (next < leftPart.peek()) {
                    middle = leftPart.poll();
                    leftPart.add(next);
                }
                //System.out.println(leftPart + " " + middle + " " + rightPart);
                System.out.printf("%.1f\n", (double) middle);
            }


        }
    }
}
