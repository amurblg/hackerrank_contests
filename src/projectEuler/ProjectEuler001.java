/*
* https://www.hackerrank.com/contests/projecteuler/challenges/euler001
* */

package projectEuler;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class ProjectEuler001 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
//        Scanner s = new Scanner("2\n" +
//                "10\n" +//3+5+6+9=23
//                "100\n");
        int t = Integer.parseInt(s.next());
        int[][] ns = new int[t][2];
        for (int i = 0; i < t; i++) {
            ns[i][0] = Integer.parseInt(s.next());
            ns[i][1] = i;
        }
        long[] result = new long[t];
        for (int i = 0; i < t; i++) {
            int n3 = (ns[i][0] - 1) / 3;
            int n5 = (ns[i][0] - 1) / 5;
            int n15 = (ns[i][0] - 1) / 15;
            result[i] = (n3 * (3 + 3 * (n3)) / 2) + (n5 * (5 + 5 * (n5)) / 2) - (n15 * (15 + 15 * (n15)) / 2);
        }
        for (int i = 0; i < t; i++) {
            System.out.println(result[i]);
        }

/*
        Arrays.sort(ns, Comparator.comparingInt(o -> o[0]));

        long start = 0;
        long prevSum = 0L;
        long result[] = new long[t];
        for (int i = 0; i < t; i++) {
            long sum = prevSum;
            for (long j = start; j < ns[i][0]; j++) {
                if (j % 3 == 0 || j % 5 == 0) sum += j;
            }
            result[(int) ns[i][1]] = sum;
            start = ns[i][0];
            prevSum = sum;
        }

        for (int i = 0; i < t; i++) {
            System.out.println(result[i]);
        }
*/
    }
}
