/*
* https://www.hackerrank.com/contests/projecteuler/challenges/euler002/problem
* */

package projectEuler;

import java.util.Scanner;

public class ProjectEuler002 {
    private static long[][] data = new long[2][100];
    /*
    2-dimensional matrix: 2 rows, unidentified number of columns.
    1st row of each column stores next Fibonacci number,
    2nd row of this column - sum of all previous even Fibonacci numbers (including this, if its even)
    */
    private static int lastElement = 1;//Last calculated element of the matrix above.

    static {
        data[0][0] = 1;
        data[1][0] = 0;
        data[0][1] = 1;
        data[1][1] = 0;
    }

    public static void main(String[] args) {
        fillDataArray((long) 1e17);
        //Scanner s = new Scanner(System.in);
        Scanner s = new Scanner("2\n" +
                "10\n" +
                "100");
        long a = s.nextLong();
        for (long i = 0; i < a; i++) {
            long nextLimit = s.nextLong();
            for (int j = 0; j < lastElement; j++) {
                if (data[0][j] > nextLimit) {
                    System.out.println(data[1][j - 1]);
                    break;
                }
            }
        }
    }

    public static void fillDataArray(long limit) {
        int nextIndex = lastElement + 1;
        while (true) {
            data[0][nextIndex] = data[0][nextIndex - 2] + data[0][nextIndex - 1];
            if (data[0][nextIndex] % 2 == 0) {
                data[1][nextIndex] = data[1][nextIndex - 1] + data[0][nextIndex];
            } else {
                data[1][nextIndex] = data[1][nextIndex - 1];
            }
            if (data[0][nextIndex] > limit) {
                lastElement = nextIndex;
                break;
            }
            nextIndex++;
        }
    }
}
