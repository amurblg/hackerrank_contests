/*
* https://www.hackerrank.com/contests/projecteuler/challenges/euler012/problem
* */

package projectEuler;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ProjectEuler012 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        List<TriangleNumber> list = new ArrayList<>();
        int t = s.nextInt();
        for (int i = 0; i < t; i++) {
            int n = s.nextInt();
            if (list.size() > 0) {
                if (list.get(list.size() - 1).divisors > n) {
                    for (int j = list.size() - 1; j >= 0; j--) {
                        if (list.get(j).divisors <= n) {
                            System.out.println(list.get(j + 1).number);
                            continue;
                        }
                    }
                }
            }
        }
    }

    private static int countDivisors(Long number) {
        if (number == 1) return 1;
        int result = 2;
        for (int i = 2; i <= number / 2; i++) {
            if (number % i == 0) result++;
        }
        return result;
    }

    private class TriangleNumber implements Comparable<TriangleNumber> {
        Long number;
        int divisors;

        public TriangleNumber(Long number, int divisors) {
            this.number = number;
            this.divisors = divisors;
        }

        @Override
        public int compareTo(TriangleNumber o) {
            if (divisors == o.divisors) return Long.compare(number, o.number);
            return divisors - o.divisors;
        }
    }
}
