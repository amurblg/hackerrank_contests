package projectEuler;

import java.math.BigInteger;
import java.util.Scanner;

public class ProjectEuler013 {
    public static void main(String[] args) {
        //Scanner s = new Scanner(System.in);
        Scanner s = new Scanner("5\n" +
                "37107287533902102798797998220837590246510135740250\n" +
                "46376937677490009712648124896970078050417018260538\n" +
                "74324986199524741059474233309513058123726617309629\n" +
                "91942213363574161572522430563301811072406154908250\n" +
                "23067588207539346171171980310421047513778063246676");
        int n = Integer.parseInt(s.nextLine());
        BigInteger result = BigInteger.ZERO;
        for (int i = 0; i < n; i++) {
            BigInteger summand = new BigInteger(s.nextLine());
            result = result.add(summand);
        }
        System.out.println(result.toString().substring(0, 10));
    }
}
