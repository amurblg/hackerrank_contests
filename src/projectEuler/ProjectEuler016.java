package projectEuler;

import java.math.BigInteger;
import java.util.Scanner;

/*
* https://www.hackerrank.com/contests/projecteuler/challenges/euler016
* */
public class ProjectEuler016 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int t = Integer.parseInt(s.nextLine());
        for (int i = 0; i < t; i++) {
            System.out.println(getDigitsSum(Integer.parseInt(s.nextLine())));
        }
    }

    public static int getDigitsSum(int powerOfTwo) {
        BigInteger bi = BigInteger.ONE;
        String number = bi.shiftLeft(powerOfTwo).toString();
        int result = number.chars().reduce(0, (a, b) -> a + b - 48);
        return result;
    }
}
