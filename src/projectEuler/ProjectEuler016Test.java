package projectEuler;

import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

public class ProjectEuler016Test {
    @Test
    public void testGetDigitsSum() {
        Random r = new Random();
        long start = System.currentTimeMillis();
        for (int i = 0; i < 100; i++) {
            ProjectEuler016.getDigitsSum(r.nextInt(10000) + 1);
        }
        System.out.printf("Finished in %dms\n", System.currentTimeMillis() - start);
    }
}