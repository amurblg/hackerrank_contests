/*
* https://www.hackerrank.com/contests/projecteuler/challenges/euler018
* */
package projectEuler;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

public class ProjectEuler018 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
/*
        Scanner s = new Scanner("1\n" +
                "4\n" +
                "3\n" +
                "7 4\n" +
                "2 4 6\n" +
                "8 5 9 3");
*/
        int t = Integer.parseInt(s.nextLine());
        for (int i = 0; i < t; i++) {
            int n = Integer.parseInt(s.nextLine());
            LinkedList<PossibleChain> queue = new LinkedList<>();
            queue.push(new PossibleChain(new int[]{Integer.parseInt(s.nextLine())}, 0));
            for (int j = 2; j <= n; j++) {
                String[] nextLine = s.nextLine().split(" ");
                while (queue.getFirst().getArray().length == nextLine.length - 1) {
                    PossibleChain prev = queue.pollFirst();
                    int[] nextArray1 = Arrays.copyOf(prev.getArray(), prev.getArray().length + 1);
                    int[] nextArray2 = Arrays.copyOf(prev.getArray(), prev.getArray().length + 1);
                    nextArray1[nextArray1.length - 1] = Integer.parseInt(nextLine[prev.getLastIndex()]);
                    nextArray2[nextArray2.length - 1] = Integer.parseInt(nextLine[prev.getLastIndex() + 1]);
                    PossibleChain next1 = new PossibleChain(nextArray1, prev.getLastIndex());
                    PossibleChain next2 = new PossibleChain(nextArray2, 1 + prev.getLastIndex());
                    queue.offerLast(next1);
                    queue.offerLast(next2);
                }
            }

            int max = Integer.MIN_VALUE;
            for (int j = 0; j < queue.size(); j++) {
                int temp = 0;
                for (int k = 0; k < queue.get(j).getArray().length; k++) {
                    temp += queue.get(j).getArray()[k];
                }
                if (temp > max) max = temp;
            }
            System.out.println(max);
        }
    }
}

class PossibleChain {
    private int[] array;
    private int lastIndex;

    public int[] getArray() {
        return array;
    }

    @Override
    public String toString() {
        return Arrays.toString(array) + "; " + lastIndex;
    }

    public int getLastIndex() {
        return lastIndex;
    }

    public PossibleChain(int[] array, int lastIndex) {

        this.array = array;
        this.lastIndex = lastIndex;
    }
}