/*
* https://www.hackerrank.com/contests/projecteuler/challenges/euler019
* */
package projectEuler;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Date;

public class ProjectEuler019 {
    public static void main(String[] args) {
        LocalDate date = LocalDate.of(1901, 1, 1);
        for (int i = 0; i < 5000; i++) {
            if (date.getDayOfWeek() == DayOfWeek.SUNDAY) System.out.print(date.getMonth().getValue() + " ");
            date = date.plusMonths(1);
        }
    }
}
