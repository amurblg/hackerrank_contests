/*
* https://www.hackerrank.com/contests/projecteuler/challenges/euler020
* */
package projectEuler;

import java.math.BigInteger;
import java.util.Scanner;

public class ProjectEuler020 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int t = Integer.parseInt(s.nextLine());
        for (int i = 0; i < t; i++) {
            calcFactorial(Integer.parseInt(s.nextLine()));
        }
    }

    public static void calcFactorial(int n) {
        BigInteger bi = BigInteger.ONE;
        for (int i = 1; i <= n; i++) {
            bi = bi.multiply(BigInteger.valueOf(i));
        }
        System.out.println(bi.toString().chars().reduce(0, (a, b) -> a + b - 48));
    }
}
