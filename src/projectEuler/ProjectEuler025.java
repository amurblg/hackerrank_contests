/*
* https://www.hackerrank.com/contests/projecteuler/challenges/euler025
* */

package projectEuler;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ProjectEuler025 {
    static List<Integer> fibLengths = new ArrayList<>();

    public static void main(String[] args) {
//        Scanner s = new Scanner(System.in);
        Scanner s = new Scanner("2\n" +
                "3\n" +
                "4");
        int t = Integer.parseInt(s.nextLine());
//        long start = System.currentTimeMillis();
        fillFibLengthsList();
//        System.out.println("Finished in " + (System.currentTimeMillis() - start) + " ms");
//        System.out.println("Total number of elements: " + fibLengths.size());

        for (int i = 0; i < t; i++) {
            int n = Integer.parseInt(s.next());
            for (int j = 0; j < fibLengths.size(); j++) {
                if (n == fibLengths.get(j)) {
                    System.out.println(j + 1);
                    break;
                }
            }
        }
    }

    public static void fillFibLengthsList() {
        BigInteger a = BigInteger.ONE;
        BigInteger b = BigInteger.ONE;
        fibLengths.add(a.toString().length());
        fibLengths.add(b.toString().length());
        int t = 1;
        while (t < 5000) {
            BigInteger c = a.add(b);
            t = c.toString().length();
            fibLengths.add(t);
            a = b;
            b = c;
        }
    }
}
