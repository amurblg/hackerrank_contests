/*
* https://www.hackerrank.com/contests/projecteuler/challenges/euler034
* */

package projectEuler;

import java.util.Scanner;

public class ProjectEuler034 {
    public static void main(String[] args) {
        int[] factorials = new int[10];
        factorials[0] = 1;
        for (int i = 1; i < 10; i++) {
            factorials[i] = i * factorials[i - 1];
        }

        Scanner s = new Scanner(System.in);
        int n = Integer.parseInt(s.nextLine());

        int total = 0;
        for (int i = 10; i < n; i++) {
            long factSum = 0;
            for (int i1 : getDigits(i)) {
                factSum += factorials[i1];
            }
            if (factSum % i == 0) total += i;
        }

        System.out.println(total);
    }

    private static int[] getDigits(int n) {
        int[] result = new int[String.valueOf(n).length()];
        int i = 0;
        while (n > 0) {
            int digit = n % 10;
            result[i++] = digit;
            n /= 10;
        }

        return result;
    }
}
