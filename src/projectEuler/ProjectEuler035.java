package projectEuler;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ProjectEuler035 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = Integer.parseInt(s.nextLine());
        long sum = 0;

        for (int circularPrime : getCircularPrimesArray(getPrimesArray(n))) {
            sum += circularPrime;
        }

        System.out.println(sum);
    }

    private static List<Integer> getCircularPrimesArray(int[] ordinaryPrimes) {
        List<Integer> res = new ArrayList<>();
        for (int ordinaryPrime : ordinaryPrimes) {
            if (ordinaryPrime < 10) {
                res.add(ordinaryPrime);
                continue;
            }
            String delta = String.valueOf(ordinaryPrime);
            boolean allPrimes = true;
            for (int i = 0; i < delta.length() - 1; i++) {
                delta = delta.substring(1) + delta.substring(0, 1);
                int circularNumber = Integer.parseInt(delta);
                if (circularNumber < ordinaryPrimes[ordinaryPrimes.length-1]) {
                    if (Arrays.binarySearch(ordinaryPrimes, circularNumber) < 0) allPrimes = false;
                } else {
                    BigInteger bi = BigInteger.valueOf(circularNumber);
                    allPrimes = bi.isProbablePrime(1);
                }
                if (!allPrimes) break;
            }
            if (allPrimes) res.add(ordinaryPrime);
        }

        return res;
    }

    private static int[] getPrimesArray(int upperLimit) {
        boolean[] booleans = new boolean[upperLimit];
        for (int i = 0; i < upperLimit; i++) {
            booleans[i] = false;
        }
        booleans[2] = booleans[3] = true;

        int x2, y2;
        int sqr_lim = (int) Math.sqrt(upperLimit);
        int n;

        x2 = 0;
        for (int i = 1; i <= sqr_lim; i++) {
            x2 += 2 * i - 1;
            y2 = 0;
            for (int j = 1; j <= sqr_lim; j++) {
                y2 += 2 * j - 1;
                n = 4 * x2 + y2;
                if (n <= upperLimit && (n % 12 == 1 || n % 12 == 5)) {
                    booleans[n] = !booleans[n];
                }
                n -= x2;
                if (n <= upperLimit && n % 12 == 7) booleans[n] = !booleans[n];
                n -= 2 * y2;
                if ((i > j) && (n <= upperLimit) && (n % 12 == 11)) booleans[n] = !booleans[n];
            }
        }
        for (int i = 5; i < sqr_lim; i++) {
            if (booleans[i]) {
                n = i * i;
                for (int j = n; j < upperLimit; j += n) {
                    booleans[j] = false;
                }
            }
        }

        List<Integer> listPrimes = new ArrayList<>();
        for (int i = 0; i < upperLimit; i++) {
            if (booleans[i]) listPrimes.add(i);
        }
        int[] primes = new int[listPrimes.size()];
        for (int i = 0; i < primes.length; i++) {
            primes[i] = listPrimes.get(i);
        }

        return primes;
    }
}
