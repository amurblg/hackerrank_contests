/*
* https://www.hackerrank.com/contests/projecteuler/challenges/euler036
* */

package projectEuler;

import java.util.Scanner;

public class ProjectEuler036 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int k = s.nextInt();

        long sum = 0;
        for (int i = 1; i < n; i++) {
            String n10 = String.valueOf(i);
            String nK = Integer.toString(i, k);
            String n10rev = new StringBuilder(n10).reverse().toString();
            String nKrev = new StringBuilder(nK).reverse().toString();
            while (n10rev.startsWith("0")) n10rev = n10rev.substring(1);
            while (nKrev.startsWith("0")) nKrev = nKrev.substring(1);
            if (n10.equals(n10rev) && nK.equals(nKrev)) {
                sum += i;
            }
        }

        System.out.println(sum);
    }
}
