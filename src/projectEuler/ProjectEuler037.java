/*
* https://www.hackerrank.com/contests/projecteuler/challenges/euler037
* */

package projectEuler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ProjectEuler037 {
    public static void main(String[] args) {
        //Scanner s = new Scanner(System.in);
        Scanner s = new Scanner("100");
        int n = s.nextInt();

        long sum = 0;
        for (Integer truncablePrime : getTruncablePrimes(getPrimes(n))) {
            sum += truncablePrime;
        }

        System.out.println(sum);
    }

    private static List<Integer> getTruncablePrimes(int[] primes) {
        List<Integer> result = new ArrayList<>();
        for (int prime : primes) {
            if (prime < 10) continue;
            String primeInString = String.valueOf(prime);
            boolean truncable = true;
            for (int i = 0; i < primeInString.length() - 1; i++) {
                truncable &= Arrays.binarySearch(primes, Integer.parseInt(primeInString.substring(0, i + 1))) >= 0;
            }
            if (!truncable) continue;
            for (int i = 0; i < primeInString.length() - 1; i++) {
                truncable &= Arrays.binarySearch(primes, Integer.parseInt(primeInString.substring(i + 1))) >= 0;
            }
            if (truncable) result.add(prime);
        }

        return result;
    }

    private static int[] getPrimes(int limit) {
        boolean[] booleans = new boolean[limit];
        for (int i = 0; i < limit; i++) {
            booleans[i] = false;
        }
        booleans[2] = booleans[3] = true;

        int x2, y2;
        int sqr_lim = (int) Math.sqrt(limit);
        int n;

        x2 = 0;
        for (int i = 1; i <= sqr_lim; i++) {
            x2 += 2 * i - 1;
            y2 = 0;
            for (int j = 1; j <= sqr_lim; j++) {
                y2 += 2 * j - 1;
                n = 4 * x2 + y2;
                if (n <= limit && (n % 12 == 1 || n % 12 == 5)) {
                    booleans[n] = !booleans[n];
                }
                n -= x2;
                if (n <= limit && n % 12 == 7) booleans[n] = !booleans[n];
                n -= 2 * y2;
                if ((i > j) && (n <= limit) && (n % 12 == 11)) booleans[n] = !booleans[n];
            }
        }
        for (int i = 5; i < sqr_lim; i++) {
            if (booleans[i]) {
                n = i * i;
                for (int j = n; j < limit; j += n) {
                    booleans[j] = false;
                }
            }
        }

        List<Integer> listPrimes = new ArrayList<>();
        for (int i = 0; i < limit; i++) {
            if (booleans[i]) listPrimes.add(i);
        }
        int[] result = new int[listPrimes.size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = listPrimes.get(i);
        }

        return result;
    }
}
