/*
* https://www.hackerrank.com/contests/projecteuler/challenges/euler038
* */

package projectEuler;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class ProjectEuler038 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int k = s.nextInt();

        Set<Character> etalon = new HashSet<>(Arrays.asList('1', '2', '3', '4', '5', '6', '7', '8'));
        if (k == 9) etalon.add('9');

        for (int i = 2; i < n; i++) {
            StringBuilder sb = new StringBuilder(String.valueOf(i));
            for (int j = 2; j <= k; j++) {
                sb.append(String.valueOf(i * j));
                if (sb.length() >= k) break;
            }
            if (sb.length() != k) continue;

            Set<Character> digits = new HashSet<>();
            sb.toString().chars().forEach(e -> digits.add((char) e));
            if (digits.equals(etalon)) System.out.println(i);
        }
    }
}
