package projectEuler;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * https://www.hackerrank.com/contests/projecteuler/challenges/euler010
 * Sum of Primes
 */
public class ProjectEuler10 {
    private static int[] primes;

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int t = s.nextInt();
        precalc(1000000);
        for (int i = 0; i < t; i++) {
            int n = s.nextInt();
            int j = 0;
            long sum = 0;
            while (primes[j] <= n) {
                sum += primes[j];
                j++;
            }
            System.out.println(sum);
        }
    }

    private static void precalc(int limit) {
        boolean[] booleans = new boolean[limit];
        for (int i = 0; i < limit; i++) {
            booleans[i] = false;
        }
        booleans[2] = booleans[3] = true;

        int x2, y2;
        int sqr_lim = (int) Math.sqrt(limit);
        int n;

        x2 = 0;
        for (int i = 1; i <= sqr_lim; i++) {
            x2 += 2 * i - 1;
            y2 = 0;
            for (int j = 1; j <= sqr_lim; j++) {
                y2 += 2 * j - 1;
                n = 4 * x2 + y2;
                if (n <= limit && (n % 12 == 1 || n % 12 == 5)) {
                    booleans[n] = !booleans[n];
                }
                n -= x2;
                if (n <= limit && n % 12 == 7) booleans[n] = !booleans[n];
                n -= 2 * y2;
                if ((i > j) && (n <= limit) && (n % 12 == 11)) booleans[n] = !booleans[n];
            }
        }
        for (int i = 5; i < sqr_lim; i++) {
            if (booleans[i]) {
                n = i * i;
                for (int j = n; j < limit; j += n) {
                    booleans[j] = false;
                }
            }
        }

        List<Integer> listPrimes = new ArrayList<>();
        for (int i = 0; i < limit; i++) {
            if (booleans[i]) listPrimes.add(i);
        }
        primes = new int[listPrimes.size()];
        for (int i = 0; i < primes.length; i++) {
            primes[i] = listPrimes.get(i);
        }
    }

}
