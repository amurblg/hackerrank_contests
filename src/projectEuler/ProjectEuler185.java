/*
* https://www.hackerrank.com/contests/projecteuler/challenges/euler185
* */

package projectEuler;

import java.util.*;

public class ProjectEuler185 {
    public static void main(String[] args) {
        //Scanner s = new Scanner(System.in);
        Scanner s = new Scanner("20\n" +
                "228569150065 1\n" +
                "907564288621 0\n" +
                "496954400043 0\n" +
                "713459943615 0\n" +
                "211421327491 1\n" +
                "258317293172 0\n" +
                "919252724339 1\n" +
                "197103476352 0\n" +
                "151173430038 0\n" +
                "063794395936 0\n" +
                "504759866532 1\n" +
                "502906565456 0\n" +
                "790539816536 0\n" +
                "595873942664 1\n" +
                "346602334981 0\n" +
                "988808475766 1\n" +
                "559203789779 0\n" +
                "498580144863 1\n" +
                "441454897857 1\n" +
                "622818801178 0");
        //The correct answer is
        //       884045122207
        int n = s.nextInt();
        s.nextLine();

        List<List<String>> possibleDigits = new ArrayList<>();
        for (int i = 0; i < 12; i++) {
            possibleDigits.add(new ArrayList<>(Arrays.asList("0", "1", "2", "3", "4", "5", "6", "7", "8", "9")));
        }

        String[][] guesses = new String[n][2];
        for (int i = 0; i < n; i++) {
            String[] nextLine = s.nextLine().split(" ");
            guesses[i][0] = nextLine[0];
            guesses[i][1] = nextLine[1];
            if ("0".equals(nextLine[1])) {
                for (int j = 0; j < 12; j++) {
                    possibleDigits.get(j).remove(nextLine[0].substring(j, j + 1));
                }
            }
        }
        printPossibleDigits(possibleDigits);
        //Current "matrix of possibilities" is
        //834020022200
        // 75 41157864
        // 8  856   97
        //i.e. first digit is 100% eight, second - either 3, or 7, or 8, third - 4 or 5, fourth - exactly 0, etc.

        int nonZeroRows = 0;
        for (int i = 0; i < n; i++) {
            if (!"0".equals(guesses[i][1])) {
                nonZeroRows++;
            }
        }
        String[][] remainDigits = new String[nonZeroRows][12];
        int[] correctDigitsInRows = new int[nonZeroRows];

        int counter = 0;
        for (int i = 0; i < n; i++) {
            if (!"0".equals(guesses[i][1])) {
                remainDigits[counter] = guesses[i][0].split("");
                correctDigitsInRows[counter] = Integer.parseInt(guesses[i][1]);
                counter++;
            }
        }

        //Now we've got matrix that consists of source input rows with correct digits other than 0. For the current example these are:
        //228569150065 1
        //211421327491 1
        //919252724339 1
        //504759866532 1
        //595873942664 1
        //988808475766 1
        //498580144863 1
        //441454897857 1
        //First 12-digit block - "remainDigits" matrix, second 1-digit block (there are only ones in it now) - "correctDigitsInRow" array
        while (!allPossibleDigitsDefined(possibleDigits)) {
            printRemainingDigitsMatrix(remainDigits);
            for (int i = 0; i < 12; i++) {
                for (int j = 0; j < remainDigits.length; j++) {
                    if (!possibleDigits.get(i).contains(remainDigits[j][i])) remainDigits[j][i] = "+";
                }
            }
            //Now matrix "remainDigits" has been transformed into this:
            //++++++15++6+ 1
            //++++21+27+9+ 1
            //+++++++2++++ 1
            //++4+++++++++ 1
            //++5+++++2+6+ 1
            //+8++++++++6+ 1
            //++++801++86+ 1
            //++++++++78+7 1
            //Once again, current "matrix of possibilities" is:
            //834020022200
            // 75 41157864
            // 8  856   97
            //Just to remind, correct answer is
            //884045122207
            for (int i = 0; i < remainDigits.length; i++) {
                int digitsCounter = 0;
                for (int j = 0; j < 12; j++) {
                    if (!"+".equals(remainDigits[i][j])) digitsCounter++;
                }
                if (digitsCounter <= correctDigitsInRows[i]) {
                    for (int j = 0; j < 12; j++) {
                        if (!remainDigits[i][j].equals("+")) {
                            for (int k = 0; k < remainDigits.length; k++) {
                                if (!remainDigits[i][j].equals(remainDigits[k][j])) remainDigits[k][j] = "+";
                            }
                            possibleDigits.get(j).clear();
                            possibleDigits.get(j).add(remainDigits[i][j]);
                        }
                    }
                }
            }
            printPossibleDigits(possibleDigits);
            for (int i = 0; i < 12; i++) {
                Set<String> uniqDigitsInColumn = new HashSet<>();
                for (int j = 0; j < remainDigits.length; j++) {
                    if (!"+".equals(remainDigits[j][i])) uniqDigitsInColumn.add(remainDigits[j][i]);
                }
                if (uniqDigitsInColumn.size() == 1) {
                    possibleDigits.get(i).retainAll(uniqDigitsInColumn);
                }
            }
            printPossibleDigits(possibleDigits);
        }
    }

    private static boolean allPossibleDigitsDefined(List<List<String>> possibleDigits) {
        int counter = 0;
        for (List<String> possibleDigit : possibleDigits) {
            counter += possibleDigit.size();
        }
        return (counter == possibleDigits.size());
    }

    private static void printPossibleDigits(List<List<String>> possibleDigits) {
        int max = 0;
        for (int i = 0; i < 12; i++) {
            if (possibleDigits.get(i).size() > max) max = possibleDigits.get(i).size();
        }
        String[][] matrix = new String[max][12];
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < max; j++) {
                matrix[j][i] = " ";
            }
            for (int j = 0; j < possibleDigits.get(i).size(); j++) {
                matrix[j][i] = possibleDigits.get(i).get(j);
            }
        }
        System.out.println("Possibilities matrix:");
        for (int i = 0; i < max; i++) {
            for (int j = 0; j < 12; j++) {
                System.out.print(matrix[i][j]);
            }
            System.out.println();
        }
    }

    private static void printRemainingDigitsMatrix(String[][] matrix) {
        System.out.println("Remaining digits matrix:");
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(matrix[i][j]);
            }
            System.out.println();
        }
    }
}
