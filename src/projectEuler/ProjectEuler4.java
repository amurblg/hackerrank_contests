package projectEuler;

import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * https://www.hackerrank.com/contests/projecteuler/challenges/euler004
 */
public class ProjectEuler4 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int t = s.nextInt();
        for (int i = 0; i < t; i++) {
            int n = s.nextInt();
            for (int j = n - 1; j >= 100001; j--) {
                String candidate = String.valueOf(j);
                if (candidate.equals(new StringBuilder(candidate).reverse().toString())) {
                    final int temp = j;
                    if (IntStream.rangeClosed(100, 999).anyMatch(e -> temp % e == 0 && temp / e < 1000)) {
                        System.out.println(j);
                        break;
                    }
                }
            }
        }
    }
}
