package projectEuler; /**
 * https://www.hackerrank.com/contests/projecteuler/challenges/euler005
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.IntStream;

public class ProjectEuler5 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();

        for (int a0 = 0; a0 < t; a0++) {
            int n = in.nextInt();

            long result = 1;
            for (int i = 1; i <= n; i++) {
                if (result < i) result *= i;
                if (result % i != 0) {
                    List<Integer> primes = getPrimeDividersList(i);
                    for (int j = 0; j < primes.size(); j++) {
                        result *= primes.get(j);
                        if (result % i == 0) break;
                    }
                }
            }
            System.out.println(result);
        }
    }

    private static List<Integer> getPrimeDividersList(int n) {
        List<Integer> result = new ArrayList<>();
        for (int i = 2; i <= n; i++) {
            int temp = i;
            if (!IntStream.range(2, temp).anyMatch(e -> temp % e == 0)) result.add(i);
        }

        for (int i = result.size() - 1; i >= 0; i--) {
            if (n % result.get(i) != 0) result.remove(i);
        }
        return result;
    }
}
