package projectEuler;

import java.util.Scanner;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

/**
 * https://www.hackerrank.com/contests/projecteuler/challenges/euler006
 * Calculate difference between sum of squares and square of sums.
 */
public class ProjectEuler6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int t = scanner.nextInt();
        for (int i = 0; i < t; i++) {
            int n = scanner.nextInt();
            long sum = IntStream.rangeClosed(1, n).sum();
            System.out.println(sum * sum - LongStream.rangeClosed(1, n).map(e -> e * e).sum());
        }

    }
}
