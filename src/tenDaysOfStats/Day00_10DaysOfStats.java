package tenDaysOfStats;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Day00_10DaysOfStats {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        //Scanner scanner = new Scanner(new String("10\n" + "64630 11735 14216 99233 14470 4978 73429 38120 51135 67060"));
        int n = scanner.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = scanner.nextInt();
        }
        Arrays.sort(array);
        System.out.println(Arrays.stream(array).boxed().mapToDouble(Integer::doubleValue).average().getAsDouble());
        System.out.println(n % 2 == 0 ? (array[n / 2 - 1] + array[n / 2]) / 2.0 : array[n / 2 - 1]);
        Map<Integer, Long> freqs = Arrays.stream(array).boxed().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        int maxFreq = Math.toIntExact(freqs.values().stream().max(Long::compareTo).get());

        for (int i = 0; i < n; i++) {
            if (freqs.get(array[i]) == maxFreq) {
                System.out.println(array[i]);
                break;
            }
        }
    }
}
