package tenDaysOfStats;

import java.util.Arrays;
import java.util.Scanner;

public class Day01_10DaysOfStats {
    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
        Scanner scanner = new Scanner(new String("10\n" +
                "10 40 30 50 20 10 40 30 50 20\n" +
                "1 2 3 4 5 6 7 8 9 10"));

        int n = scanner.nextInt();
        int[] array = new int[n];
        int[] weights = new int[n];
        int[] weightedArray = new int[n];
        double sum = 0.0;
        for (int i = 0; i < n; i++) {
            array[i] = scanner.nextInt();
        }
        for (int i = 0; i < n; i++) {
            weights[i] = scanner.nextInt();
            weightedArray[i] = array[i] * weights[i];
            sum += weights[i];
        }
        System.out.println(round(Arrays.stream(weightedArray).sum() / sum, 1));
    }

    private static double round(double number, int scale) {
        int pow = 10;
        for (int i = 1; i < scale; i++)
            pow *= 10;
        double tmp = number * pow;
        return (double) (int) ((tmp - (int) tmp) >= 0.5f ? tmp + 1 : tmp) / pow;
    }
}
