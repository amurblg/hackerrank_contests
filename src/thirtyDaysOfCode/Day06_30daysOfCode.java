package thirtyDaysOfCode;

import java.util.Scanner;

/**
 * https://www.hackerrank.com/challenges/30-review-loop
 * Day 6: Let's Review
 * 30 Days of Code
 */
public class Day06_30daysOfCode {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int i = scanner.nextInt();
        scanner.nextLine();
        for (int j = 0; j < i; j++) {
            String[] nextLine = scanner.nextLine().split("");
            for (int k = 0; k < nextLine.length; k+=2) {
                System.out.print(nextLine[k]);
            }
            System.out.print(" ");
            for (int k = 1; k < nextLine.length; k+=2) {
                System.out.print(nextLine[k]);
            }
            System.out.println();
        }
    }
}
