package thirtyDaysOfCode;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

/**
 * https://www.hackerrank.com/challenges/30-binary-numbers
 */
public class Day10_30daysOfCode {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        String[] ones = Integer.toString(s.nextInt(), 2).split("0");
        System.out.println(Arrays.stream(ones)
                .max(Comparator.comparingInt(String::length))
                .get()
                .length()
        );
    }
}
