package thirtyDaysOfCode;

import java.util.Arrays;
import java.util.Scanner;

public class Day11_30daysOfCode {
    public static void main(String[] args) {
        int[][] array = new int[6][6];

        Scanner scanner = new Scanner(System.in);
//        Scanner scanner = new Scanner(new String("1 1 1 0 0 0\n" +
//                "0 1 0 0 0 0\n" +
//                "1 1 1 0 0 0\n" +
//                "0 0 2 4 4 0\n" +
//                "0 0 0 2 0 0\n" +
//                "0 0 1 2 4 0"));

        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                array[i][j] = scanner.nextInt();
            }
        }
        int[] hourGlasses = new int[16];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                hourGlasses[i * 4 + j] = array[i][j] + array[i][j + 1] + array[i][j + 2] +
                        array[i + 1][j + 1] +
                        array[i + 2][j] + array[i + 2][j + 1] + array[i + 2][j + 2];
            }
        }
        System.out.println(Arrays.stream(hourGlasses).max().getAsInt());
    }
}
