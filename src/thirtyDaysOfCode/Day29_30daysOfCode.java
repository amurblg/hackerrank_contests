package thirtyDaysOfCode;

import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day29_30daysOfCode {
    public static void main(String[] args) {
        //Scanner s = new Scanner(System.in);
        Scanner s = new Scanner("3\n" +
                "5 2\n" +
                "8 5\n" +
                "2 2");
        int t = s.nextInt();
        for (int i = 0; i < t; i++) {
            int n = s.nextInt();
            int k = s.nextInt();

            List<Integer> lst = IntStream.rangeClosed(1, n).boxed().collect(Collectors.toList());
            int min = Integer.MAX_VALUE;
            int sum = lst.get(0) & lst.get(1);
            for (int j = 0; j < lst.size() - 1; j++) {
                for (int l = 1; l < lst.size(); l++) {
                    int bitwiseAnd = lst.get(j) & lst.get(l);
                    if (k > bitwiseAnd && k - bitwiseAnd < min) {
                        min = k - bitwiseAnd;
                        sum = bitwiseAnd;
                    }
                }
            }
            System.out.println(sum);
        }
    }
}
